#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
from distutils.version import LooseVersion

from base_install import Accessibility_Installer


class SpeechDispatcherVoxin_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall\n\tuninstall" % sys.argv[0]

    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "speech-dispatcher-voxin"
        self.speechd_conf = "/etc/speech-dispatcher/speechd.conf"

    def install(self):
        pass

    def uninstall(self):
        pass

    def upgrade(self):
        if LooseVersion(self.last_configured_version) <= LooseVersion('0.9.0'):
            self.execute_commands([
                # comment-out AddModule clause
                'sed -i "s/^[[:space:]]*AddModule[[:space:]]*.ibmtts./#&/" %s' % self.speechd_conf,
            ])


if __name__ == "__main__":
    inst = SpeechDispatcherVoxin_Installer()
    inst.launch(sys.argv)
